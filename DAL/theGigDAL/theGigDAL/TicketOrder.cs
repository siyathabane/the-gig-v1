//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace theGigDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class TicketOrder
    {
        public int ID { get; set; }
        public System.Guid UId { get; set; }
        public int TicketID { get; set; }
        public int AccountID { get; set; }
        public string Quantity { get; set; }
        public System.DateTime DateTime { get; set; }
        public int UserInfoID { get; set; }
    
        public virtual AccountDetails AccountDetails { get; set; }
    }
}
