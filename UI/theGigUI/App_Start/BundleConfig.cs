﻿using System.Diagnostics;
using System.Web;
using System.Web.Optimization;

namespace theGigUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/plugins/jquery/jquery-2.2.3.min.js")
                .Include("~/plugins/jQueryUI/jquery-ui.min.js")
                );

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/AngularApp")
                    .Include("~/Scripts/angular.js")
                    .Include("~/Scripts/angular-route.js")
                    .Include("~/Scripts/angular-ui/ui-bootstrap-tpls.js")
                    .Include("~/Scripts/angular-animate.js")
                     .Include("~/Scripts/lib/angular-input-match.js")
                .Include("~/Scripts/lib/showErrors.js")
                .Include("~/Scripts/lib/loading-bar.js")
                 .Include("~/App/AngularApp.js")
                .IncludeDirectory("~/App/Services", "*.js")
                // .IncludeDirectory("~/App/Directives", "*.js")
                .IncludeDirectory("~/App/Controllers", "*.js")
                .Include("~/plugins/datatables/jquery.dataTables.min.js")
                .Include("~/plugins/datatables/dataTables.bootstrap.min.js")
                .Include("~/plugins/slimScroll/jquery.slimscroll.min.js")
                .Include("~/plugins/fastclick/fastclick.js")
                .Include("~/Scripts/bootstrap.js")
                .Include("~/Scripts/respond.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/font-awesome.css",
                      "~/Content/ionicons.css",
                       //"~/Content/site.css",
                       "~/Content/loading-bar.css",
                       "~/dist/css/AdminLTE.css",
                       "~/plugins/datatables/dataTables.bootstrap.css",
                       "~/dist/css/skins/skin-blue.min.css"));

            BundleTable.EnableOptimizations = !Debugger.IsAttached;

        }
    }
}
