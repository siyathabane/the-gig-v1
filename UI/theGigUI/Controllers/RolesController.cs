﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using theGigDAL;

namespace theGigUI.Controllers
{
    [RoutePrefix("api/Roles")]
    public class RolesController : ApiController
    {
        private theGiGdbEntities db = new theGiGdbEntities();

        #region Retreive
        // GET: api/Roles
        public IQueryable<Roles> GetRoles()
        {
            return db.Roles;
        }

        // GET: api/Roles/5
        [ResponseType(typeof(Roles))]
        public async Task<IHttpActionResult> GetRoles(int id)
        {
            try
            {
                Roles roles = await db.Roles.FindAsync(id);
                if (roles == null)
                {
                    return NotFound();
                }

                return Ok(roles);
            }
            catch (Exception)
            {
                return BadRequest(ModelState);
            }
        } 
        #endregion

        #region Save
        // PUT: api/Roles/5
        [ResponseType(typeof(Roles))]
        public async Task<IHttpActionResult> PutRoles(Roles roles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                db.Entry(roles).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RolesExists(roles.Id))
                {
                    return NotFound();
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }

            return Ok(roles);
        }

        // POST: api/Roles
        [ResponseType(typeof(Roles))]
        public async Task<IHttpActionResult> PostRoles(Roles roles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                db.Roles.Add(roles);
                await db.SaveChangesAsync();
            }
            catch (Exception)
            {
                return BadRequest(ModelState);
            }

            return Ok(roles);
        } 
        #endregion

        #region Delete
        // DELETE: api/Roles/5
        [ResponseType(typeof(Roles))]
        public async Task<IHttpActionResult> DeleteRoles(int id)
        {
            Roles roles = await db.Roles.FindAsync(id);
            if (roles == null)
            {
                return NotFound();
            }

            try
            {
                db.Roles.Remove(roles);
                await db.SaveChangesAsync();
            }
            catch (Exception)
            {
                return BadRequest(ModelState);
            }

            return Ok(roles);
        } 
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RolesExists(int id)
        {
            return db.Roles.Count(e => e.Id == id) > 0;
        }
    }
}