﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using theGigDAL;

namespace theGigUI.Controllers
{
    [RoutePrefix("api/Userinfoes")]
    public class UserInfoesController : ApiController
    {
        private theGiGdbEntities db = new theGiGdbEntities();

        #region Retreive
        // GET: api/UserInfoes
        //public IQueryable<UserInfo> GetUserInfo()
        //{
        //    return db.UserInfo.Where(u => u.DateActive != null && u.IsLockedOut == false);
        //}

        // GET: api/UserInfoes
        //[ResponseType(typeof(List<UserInfo>))]
        public async Task<IHttpActionResult> GetUserInfo()
        {
            try
            {
                List<UserInfo> userInfo = await db.UserInfo.ToListAsync();
                if (userInfo == null)
                {
                    return NotFound();
                }

                return Ok(userInfo);
            }
            catch (Exception ex)
            {
                return BadRequest(ModelState);
            }
        }

        [Route("Count")]
        public async Task<IHttpActionResult> GetUserInfoCount()
        {
            try
            {
                int userInfo = await db.UserInfo.CountAsync();
                if (userInfo == null)
                {
                    return NotFound();
                }

                return Ok(userInfo);
            }
            catch (Exception ex)
            {
                return BadRequest(ModelState);
            }
        }

        // GET: api/UserInfoes/byType
        //[Route("byType")]
        public IQueryable<UserInfo> GetUserInfoByType(int typeID)
        {
            return db.UserInfo.Where(u => u.TypeID == typeID);
        }

        // GET: api/UserInfoes/lockedOut
        [Route("lockedOut")]
        public IQueryable<UserInfo> GetLockedOutUserInfo()
        {
            return db.UserInfo.Where(u => u.DateActive == null && u.IsLockedOut == true);
        }

        // GET: api/UserInfoes/deactivated
        [Route("deactivated")]
        public IQueryable<UserInfo> GetDeactivatedUserInfo()
        {
            return db.UserInfo.Where(u => u.DateActive != null);
        }


        // GET: api/UserInfoes/uid
        [ResponseType(typeof(UserInfo))]
        public async Task<IHttpActionResult> GetUserInfo(Guid UId)
        {
            try
            {
                UserInfo userInfo = await db.UserInfo.SingleOrDefaultAsync(u => u.Uid == UId);
                if (userInfo == null)
                {
                    return NotFound();
                }

                return Ok(userInfo);
            }
            catch (Exception)
            {
                return BadRequest(ModelState);
            }
        }
        #endregion

        #region Save
        // PUT: api/UserInfoes
        [ResponseType(typeof(UserInfo))]
        public async Task<IHttpActionResult> PutUserInfo(UserInfo userInfo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!UserInfoExists(userInfo.ID))
            {
                return NotFound();
            }
            try
            {
                db.Entry(userInfo).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                return BadRequest(ModelState);
            }

            return Ok(userInfo);
        }

        // POST: api/UserInfoes
        [ResponseType(typeof(UserInfo))]
        public async Task<IHttpActionResult> PostUserInfo(UserInfo userInfo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                userInfo.DateActive = DateTime.Now;
                userInfo.IsLockedOut = false;
                db.UserInfo.Add(userInfo);
                userInfo.Password = userInfo.Password;
                userInfo.TypeID = userInfo.TypeID;
                await db.SaveChangesAsync();

                return Ok(userInfo);
            }
            catch (Exception)
            {
                return BadRequest(ModelState);
            }
        }
        #endregion

        #region Activate/Deactivate/Lock/UnLock
        // DELETE: api/UserInfoes/5
        [ResponseType(typeof(UserInfo))]
        public async Task<IHttpActionResult> DeleteUserInfo(Guid UId)
        {
            try
            {
                UserInfo userInfo = await db.UserInfo.SingleOrDefaultAsync(u => u.Uid == UId);
                if (userInfo == null)
                {
                    return NotFound();
                }

                userInfo.IsLockedOut = !userInfo.IsLockedOut;
                await db.SaveChangesAsync();

                return Ok(userInfo);
            }
            catch (Exception)
            {
                return BadRequest(ModelState);
            }
        }

        // DELETE: api/UserInfoes/deactivate
        [Route("deactivate/UId")]
        [ResponseType(typeof(UserInfo))]
        public async Task<IHttpActionResult> DeactivateUserInfo(Guid UId)
        {
            try
            {
                UserInfo userInfo = await db.UserInfo.SingleOrDefaultAsync(u => u.Uid == UId);
                if (userInfo == null)
                {
                    return NotFound();
                }

                userInfo.DateDeactivated = DateTime.Now;
                userInfo.IsLockedOut = true;
                await db.SaveChangesAsync();

                return Ok(userInfo);
            }
            catch (Exception)
            {
                return BadRequest(ModelState);
            }
        }

        // DELETE: api/UserInfoes/activate
        [Route("activate/UId")]
        [ResponseType(typeof(UserInfo))]
        public async Task<IHttpActionResult> ActivateUserInfo(Guid UId)
        {
            try
            {
                UserInfo userInfo = await db.UserInfo.SingleOrDefaultAsync(u => u.Uid == UId);
                if (userInfo == null)
                {
                    return NotFound();
                }

                userInfo.DateActive = DateTime.Now;
                userInfo.IsLockedOut = false;
                await db.SaveChangesAsync();

                return Ok(userInfo);
            }
            catch (Exception)
            {
                return BadRequest(ModelState);
            }
        } 
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserInfoExists(int id)
        {
            return db.UserInfo.Count(e => e.ID == id) > 0;
        }
    }
}