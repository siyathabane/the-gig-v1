﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using theGigDAL;
using theGigUI.Models;

namespace theGigUI.Controllers
{
    [RoutePrefix("Account")]
    public class AccountController : ApiController
    {
        private theGiGdbEntities db = new theGiGdbEntities();

        // GET: api/Account
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Account/5
        [HttpPost]
        [Route("Login")]
        public async Task<IHttpActionResult> Login(LoginViewModel user)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await db.UserInfo.SingleOrDefaultAsync(x => x.Email == user.UserName || x.UserName == user.UserName);

                if (result == null || result.Password != Cipher.Encrypt(user.Password))
                {
                    if (result != null)
                    {
                        if (result.FailedLoginAttempts >= 3)
                            result.IsLockedOut = true;
                    }

                }
                else
                {
                    result.FailedLoginAttempts = result.FailedLoginAttempts + 1;
                    return BadRequest("Invalid user name and password combination.");
                }

                if (result.DateDeactivated != null)
                    return BadRequest("Account Deactivated!");

                if (result.IsLockedOut == true)
                    return BadRequest("Account locked out!");

                //result.LastLogin = DateTime.Now;

                if (result.FailedLoginAttempts > 0)
                    result.FailedLoginAttempts = 0;


                await db.SaveChangesAsync();

                return Ok(result);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message.ToString());

            }
        }

        // POST: api/Account
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Account/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Account/5
        public void Delete(int id)
        {

        }
    }
}

