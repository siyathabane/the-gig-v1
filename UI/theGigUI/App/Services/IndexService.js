﻿AngularApp.factory('IndexService',
    ['$http',
        function ($http) {

            //Local methods

            //Api calls
            var getUsers = function (){
              return $http.get("/api/UserInfoes");
            }


            //Exposing methods
            return{
                getUsers: getUsers
            }
}]);