﻿AngularApp.factory('Security',
   ['$rootScope',  function ($rootScope) {

       var _scopeUpdateEvent = 'current-user-updated';

       var _CurrentUser = {
           loggedIn: false,
           id: 0,
           userName: '',
           displayName: '',
           allowedPrivileges: null,
           userType: ''
       };

       var login = function (user) {
           _CurrentUser = user;

           $rootScope.$emit(_scopeUpdateEvent, _CurrentUser);

       };

       var logout = function () {
           _CurrentUser.id = 0;
           _CurrentUser.userName = '';
           _CurrentUser.displayName = '';
           _CurrentUser.userType = '';
           _CurrentUser.isLoggedIn = false;

           $rootScope.$emit(_scopeUpdateEvent, _CurrentUser);

       };

       $rootScope.$emit(_scopeUpdateEvent, _CurrentUser);

       //var _userHasPrivileges = function () {
       //    if (!_CurrentUser || _CurrentUser.allowedPrivileges == null)
       //        return false;

       //    if (!_CurrentUser.allowedPrivileges.length > 0)
       //        return false;

       //    return true;
       //}

       //var _isAllowed = function (privilegeType) {

       //    if (!_userHasPrivileges())
       //        return false;

       //    var allValues = EnumsService.securityTypes();
       //    var myEnum = -1;
       //    for (x = 0; x < allValues.length; x++) {
       //        if (allValues[x].name == privilegeType) {
       //            myEnum = allValues[x].ordinalValue;
       //            break;
       //        }
       //    }

       //    if (myEnum < 0)
       //        return false;


       //    var result = _CurrentUser.allowedPrivileges.indexOf(myEnum) > -1;
       //    return result;

       //}

       return {
           currentUser: _CurrentUser,
           login: login,
           logout: logout,
           scopeUpdateEvent: _scopeUpdateEvent,
           //isAllowed: _isAllowed,
           //userHasPrivileges: _userHasPrivileges
       }


   }
   ]);