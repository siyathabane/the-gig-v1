﻿AngularApp.factory('AccountService',
    ['$http',
    function ($http) {
        var login = function (loginDetails) {
            return $http.post("/Account/Login", loginDetails);
        };

        var register = function (user) {
            return $http.post("/Account/Register", user);
        };

        var currentUser = function () {
            return $http.get('/Account/GetCurrentUser');
        }

        var singOut = function () {
            return $http.post('/Account/SignOut', { Id: 1 });
        }

        return {
            login: login,
            register: register,
            currentUser: currentUser,
            singOut: singOut,
        }
    }

    ]);

