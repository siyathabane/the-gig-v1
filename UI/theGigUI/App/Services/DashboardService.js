﻿AngularApp.factory('DashboardService',
    ['$http',
        function ($http) {

            //Local methods

            //Api calls
            var getUserCount = function (){
              return $http.get("/api/UserInfoes/Count");
            }


            //Exposing methods
            return{
                getUserCount: getUserCount
            }
}]);