﻿AngularApp.controller('DashboardController',
[
    '$scope', '$rootScope', '$window', 'DashboardService',
     function ($scope, $rootScope, $window, DashboardService) {
         $scope.applicationReady = true;
         $scope.userCount = 0;
         $scope.navbarProperties = {
             isCollapsed: true
         };


         $scope.getUserCount = function () {
             DashboardService.getUserCount()
             .then(
             function (result) {
                 $scope.userCount = result.data;
             },
             function (error) {
                 alert("an error occured : unable to get data");
             });
         }

         $scope.getTotals = function(){
             $scope.getUserCount();
         }

     }]);