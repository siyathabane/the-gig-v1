﻿AngularApp.controller('IndexController',
[
    '$scope', '$rootScope', '$window', 'IndexService',
     function ($scope, $rootScope, $window, IndexService) {
         $scope.applicationReady = true;
         $scope.users = [];
         $scope.navbarProperties = {
             isCollapsed: true
         };

         $scope.user = {
          isLoggedIn : false,
          isAdmin : false,
          roles : []
         };


         $scope.getUsers = function () {
             IndexService.getUsers()
             .then(
             function (result) {
                 $scope.users = result.data;
             },
             function (error) {
                 alert("an error occured : unable to get data");
             });
         }

     }]);