﻿AngularApp.controller('CrudController',
[
    '$scope', '$rootScope', '$window', 'CrudService',
     function ($scope, $rootScope, $window, CrudService) {

     ////Variables
         $scope.applicationReady = true;
         $scope.list = [];
         $scope.navbarProperties = {
             isCollapsed: true
         };
         $scope.search = ''

////////////////////////////////////////////////////////////////// Local Methods /////////////////////////////////////




////////////////////////////////////////////////////////////////// Service calls /////////////////////////////////////

         $scope.getList = function (controller) {
             CrudService.getList(controller)
             .then(
             function (result) {
                 $scope.list = result.data;
             },
             function (error) {
                 alert("an error occured : unable to get data");
             });
         }

     }]);