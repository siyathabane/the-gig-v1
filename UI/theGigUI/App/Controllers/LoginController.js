﻿AngularApp.controller('LoginController',
            ['$scope', '$location', 'AccountService', 'Security', 
    function ($scope, $location, AccountService, Security) {

        $scope.loginModel = {
                        userName: '',
                        password: '',
                        rememberMe: false
                    };

        $scope.currentUser = {
                    loggedIn:false,
                    id: null,
                    username:'',
                    displayName:'',
                    roles:[],
                    userType:''
        };
       

        //$scope.userTypes = EnumsService.userTypes();

        $scope.login = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.loginForm.$valid) {

                AccountService.login($scope.loginModel).then(

                    function (result) {

                        Security.login(result.data);
                       // window.location = "/#/dashboard";
                        $location.path('/dashboard');



                        //if ($scope.userTypes[0].description == result.data.userTypeString) {
                        //    $location.path('/home');
                        //}
                        //else if ($scope.userTypes[1].description == result.data.userTypeString) {
                        //    $location.path('/doctorHome');
                        //}
                        //else if ($scope.userTypes[2].description == result.data.userTypeString) {
                        //    $location.path('/patientHome');
                        //}
                    },

                    function (error) {
                        $scope.hasError = true;
                        $scope.errorMessage = error.statusText;
                        Security.logout();
                    }

                    );
            }
        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }
    }

    ]);

